# Attiny45 BOOST voltage

This is also compatible to the AtTiny85 model. _(AtTiny85 is better having more possibilities & better limits)_

Make an Attiny45 driven voltage boost STEP-UP, able to power up a led projector from a little 7.3v lipo battery.
The output is not made to be high power, but a quite high voltage enough to make it bright correctly.

## Getting started

This Step up is design to drive, a 10/12 LED panel adjusting the brightness from a potentiometer.

This makes the PWM duty cycle to vary, & so the STEP-UP output is adjusted in voltage & LED are more or less bright.

This might be quite useless method for just driving LED panel brightness, but the real goal is not to build it, but 
to learn how to do that as a proof of concept.

## Tools

I m using Kicad 6.x for my schematic & PCB design & gerber files creation.

I use arduino IDE or VS-code to push compiled code to the ATtiny shield over an arduino uno R3 to the ROM of the Attiny microcontroler.

All components are chosen from my own stock so you can use similar components as far as you recalculate the values to be sure it will
still work fine.

## LICENSE

See the [Creative Commons BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode) to keep it very open.

## Files

[PCB](./PCB.pdf)

[ATTiny](./AtTiny.pdf)

[LED-SETUP](./LED_STEPUP/LED_STEPUP.pdf)

[REGULATOR](./AZ1117-5_to263.pdf)

## Pics

![component side](./LED_STEPUP_compts.png)

![copper side](./LED_STEPUP_copper.png)

![schematic](./LED_SETUP_sch.png)

## Code 

```c

// Needs ATtiny45 libs
// Needs to follow https://docs.arduino.cc/tutorials/generic/secrets-of-arduino-pwm
// as concept & apply it to the ATtiny (here I use an AtTiny45 model).

int led = 0;			// Led control is wired to the 0 pin 
int PWM_VAL_MAX = 159; 		//  sounds like about 100kHz hashing frequency
int MAX_LED_ADC_ratio = 186; 	// change this to adjust LED_bright
int PLLCSR = 6;			// here an below vairables are related to setup of the PWM itself in high frequency mode
int TCCR1 = 227;  		// 11100011 PCK/4 need for high frequencies PWM (see datasheet about clock select bits 3 - 0 etc.) 
int GTCCR = 0;			// got references from https://playground.arduino.cc/Main/TimerPWMCheatsheet/
int OCR1C = PWM_VAL_MAX; 	// so here is the real main value to adjust higher/lower pwm duty cycle at end on each control loop

void setup() {                
  pinMode(led, OUTPUT);    
  pinMode(1, OUTPUT);
  analogWrite(1, LOW);
}

void loop() {
  analogReference(DEFAULT);
  static long time_ref = 0;
  static int LED_bright = 0;
  static int UD_STATE = 0;
  if(millis() > time_ref) {	// reads variation
	time_ref = millis() + 1;
	int buffered_val_analog = analogRead(A2);
	delayMicroseconds(5);
	buffered_val_analog = analogRead(A2);
  }
  static int pwmbuffered_val_analog = 0;
  analogReference(INTERNAL);	// do not forget to change reference here to INTERNAL
  int buffered_val_analog = analogRead(A3); 
  delayMicroseconds(5);		// might be enough
  buffered_val_analog = analogRead(A3);
  // define comparing value as ADC ratio
  int adjustement_var = ((MAX_LED_ADC_ratio * buffered_val_analog) / 255);	// not sure it is really need that precision
  buffered_val_analog = (analogRead(A3) * 3) / 4 ;				// compare threshold		
  if(buffered_val_analog < adjustement_var) {
	if(pwmbuffered_val_analog < (PWM_VAL_MAX-20)) pwmbuffered_val_analog++;	// adjustment to high
	} else {
		if(pwmbuffered_val_analog > 0) pwmbuffered_val_analog--;	// adjustement to low
	}
	OCR1A = pwmbuffered_val_analog;
}
```
